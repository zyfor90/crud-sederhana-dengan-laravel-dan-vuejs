const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js');

mix.styles([
   'resources/css/bootstrap.css',
   'resources/js/vendor/font-awesome/css/font-awesome.css',
   'resources/js/vendor/linearicons/style.css',
   'resources/css/main.css',
], 'public/css/app.css');


if (mix.inProduction()) {
  mix.version()
}

mix.sourceMaps();
